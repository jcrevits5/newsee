# newsee

## Setup
1. create a virtual environment (if you want)
2. set the API key environment variable -> `export NEWS_API_KEY=<value>`
3. `pip install requirements.txt`

## Starting the server
1. run migrations - `python manage.py migrate`
2. populate the database - `python manage.py populate`
3. start the server - `python manage.py runserver`
4. access http://localhost:8000/assignment/ in your favorite browser

## Running tests
If I had more time I'd implement some tests using `from django.test import Client`, looks fairly simple!
