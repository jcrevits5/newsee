import logging
import os
from typing import Any, Dict, List

import requests


NEWS_API_KEY = os.environ.get('NEWS_API_KEY')
NEW_API_BASE_URL = 'https://newsapi.org'


def top_headlines() -> List[Dict[str, Any]]:
    if not NEWS_API_KEY:
        logging.error('Please set the NEWS_API_KEY environment var')
        return []

    try:
        response = requests.get(
            url=f'{NEW_API_BASE_URL}/v2/top-headlines',
            params={
                'apiKey': NEWS_API_KEY,
                'country': 'us',
            })
        response.raise_for_status()
    except Exception:
        logging.exception('Error while fetching top headlines')
        return []

    return response.json()['articles']
