from assignment.libs.newsapi import top_headlines
from assignment.models import News

from django.core.management.base import BaseCommand
from django.db import IntegrityError


class Command(BaseCommand):
    help = 'Populates the database with news items'

    def handle(self, *args, **options):
        news_items = top_headlines()

        self.stdout.write(f'Inserting {len(news_items)} news items')
        inserted_items_counter = 0
        skipped_items_counter = 0

        for item in news_items:
            try:
                News(
                    title=item['title'],
                    author=item['author'],
                    content=item['content'],
                    article_url=item['url'],
                    image_url=item['urlToImage'],
                    published_at=item['publishedAt'],
                    source=item['source']['name'],
                ).save()
            except IntegrityError as e:
                if str(e) == 'UNIQUE constraint failed: assignment_news.article_url':
                    skipped_items_counter += 1
                    continue

                self.stderr.write(f'Encountered unexpected integrity error - {e}')
                continue
            except Exception as e:
                self.stderr.write(f'Something went wrong with this insertion - {e}')
                continue

            self.stdout.write(f'Inserted - {item["title"]}')
            inserted_items_counter += 1

        if skipped_items_counter > 0:
            self.stdout.write(self.style.SUCCESS(f'Skipped {skipped_items_counter} items'))

        self.stdout.write(self.style.SUCCESS(f'Inserted {inserted_items_counter} items'))
