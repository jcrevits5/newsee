from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('most-recent', views.most_recent),
    path('latest-news', views.NewsViews.as_view()),
]
