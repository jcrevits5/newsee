from typing import Any, Dict

from django.db import models


class News(models.Model):
    title = models.TextField()
    author = models.TextField(null=True, db_index=True)
    content = models.TextField(null=True)
    article_url = models.TextField(unique=True, db_index=True)
    image_url = models.TextField()
    published_at = models.DateTimeField(db_index=True)
    source = models.TextField(null=True)

    @classmethod
    def most_recent(
            self,
            count: int,
            filters: Dict[str, str] = {},
    ) -> models.query.QuerySet:
        return self.objects.filter(**filters).order_by('-published_at')[:count]

    def to_dict(self) -> Dict[str, Any]:
        return {
            'id': self.id,
            'title': self.title,
            'author': self.author,
            'content': self.content,
            'article_url': self.article_url,
            'image_url': self.image_url,
            'published_at': self.published_at,
            'source': self.source,
        }
