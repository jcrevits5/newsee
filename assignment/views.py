from django.http import JsonResponse
from django.shortcuts import render
from django.views import View

from assignment.models import News


def index(request):
    return render(request, 'assignment/index.html')


def most_recent(request):
    new_items = [item.to_dict() for item in News.most_recent(100)]
    return JsonResponse(
        data={'articles': new_items},
        safe=False,
    )


class NewsViews(View):

    def get(self, request):
        filters = {}

        try:
            filters['author'] = request.GET['author'] or None
        except KeyError:
            pass

        context = {
            'news_items': News.most_recent(
                count=20,
                filters=filters,
            ),
        }

        return render(request, 'assignment/news.html', context)
